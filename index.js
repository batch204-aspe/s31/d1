/* 	Use the "require" directive to load Node.js modules

	"http module"
		► lets Node.js transfer data using HyperText Transfer protocol

		► is a set of individual files that contains code to create a component that helps establish data transfer between applications

		► is a protocol that allows the fetching of resources such as HTML documents



*/

let http = require ("http");

// The http module has a createServer() methods that accepts a function as an argument and aloows for a creation of a server

// A port is a virtual point where network connections start and end.

// The server will be assigned to port "4000" via the "listen(4000)" method where the server will listen to any request that are sent to it eventually communication with our server.

http.createServer(function (request, response) {


	
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World!');

}).listen(4000)
// When a server is running, console will print the message at the terminal
console.log("Server Running at localhost:4000");