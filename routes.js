
const http = require('http');

// Creating a variable "port" to store the port number
const port = 4000;

// Creates a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response) => {

	if(request.url == '/greeting') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Hello from the Other Side!...
			This is the Greeting Page`);
	}

	else if (request.url == '/home') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`This is the homepage!...`);
	}

	else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("Page not available");
	}
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);




















